const elLaptops = document.getElementById("laptops");
const elPrice = document.getElementById("price");
const elDescription = document.getElementById("description");
const elSpecs = document.getElementById("specs");
const elTitle = document.getElementById("title");
const elBalance = document.getElementById("balance");
const elLoan = document.getElementById("loan");
const elPay = document.getElementById("pay");
const btnLoan = document.getElementById("btn-loan");
const btnBank = document.getElementById("btn-bank");
const btnWork = document.getElementById("btn-work");
const btnBuy = document.getElementById("btn-buy");
const btnRepayLoan = document.getElementById("btn-repay-loan");
const elImage = document.getElementById("image");

let laptops = [];
let balance = 200;
let loan = 0;
let hasLoan = false;
let pay = 0;
let image = 'https://noroff-komputer-store-api.herokuapp.com/';


// Joe bankers balance - updating joes balance based on loan amount
const handleBalance =() => {
    if(hasLoan == false) {
        const getLoan = prompt("Please enter the amount of money you want to loan: ");
        if (getLoan <= balance*2) {
            balance += parseInt(getLoan);
    loan += parseInt(getLoan);
    elBalance.innerText = `Balance: ${balance} Kr.`;
    elLoan.innerText = `Outstanding loan: ${loan} kr.`;
    hasLoan = true;
    //showing the repay loan button
    btnRepayLoan.style.visibility = "visible";
        } else {
            alert("You cannot get a loan thats more than double of your balance");
        }
    } else {
        alert("You already have a loan, buy a computer or pay it down before you get a new loan");
    }
}

// Adding 100kr to the Pay balance when clicking the Work button
const handleWorkButton = () => {
    pay += 100;
    elPay.innerText = `Pay: ${pay} Kr.`
}

// Transfering from pay balance to bank balance with Bank button
const handleBankTransfer = () => {
    //checking if joe has a loan
    if(hasLoan == true) {
        const deduction = 0.1 * pay;
        //checking if the loan is bigger than the 10% deduction
        if (loan >= deduction) {
            loan -= deduction;
            pay -= deduction;
            balance += pay;
            pay = 0;
        } else {
            pay -= loan;
            loan -= loan;
            balance += pay;
            pay = 0;
        }
        elBalance.innerText = `Balance: ${balance} Kr.`;
        elLoan.innerText = `Outstanding loan: ${loan} kr.`;
        elPay.innerText = `Pay: ${pay} Kr.`
    } else {
        balance += pay;
        elBalance.innerText = `Balance: ${balance} Kr.`;
        pay = 0;
        elPay.innerText = `Pay: ${pay} Kr.`
    }

}

//Repay loan button - full value of current Pay amount goes towards the outstanding loan
const handleRepayLoan = () => {
    //If the pay is larger than the loan, the rest will be trasfered to bank balance.
    if(pay > loan) {
        balance -= loan;
        pay -= loan;
        loan -= loan;
        balance += pay;
        pay = 0;
        elBalance.innerText = `Balance: ${balance} Kr.`;
        elLoan.innerText = `Outstanding loan: ${loan} kr.`;
        elPay.innerText = `Pay: ${pay} Kr.`;
    //If the pay is not greater the loan will be partially payed down.
    } else {
        balance -= pay;
        loan -= pay;
        pay = 0;
        elBalance.innerText = `Balance: ${balance} Kr.`;
        elLoan.innerText = `Outstanding loan: ${loan} kr.`;
        elPay.innerText = `Pay: ${pay} Kr.`;
    }
    //Hiding the repay loan button when there is no more outstanding loan.
    if (loan == 0) {
        hasLoan = false;
        btnRepayLoan.style.visibility = "hidden";

    }
}


// API - fetching data about the laptops
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));


//going through each laptop in the api
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    elPrice.innerText = laptops[0].price;
    elDescription.innerText = laptops[0].description;
    elTitle.innerText = laptops[0].title;
    elSpecs.innerText = laptops[0].specs;
    
}
const addLaptopToMenu = (laptop) => {
    const elLaptop = document.createElement("option");
    elLaptop.value = laptop.id;
    elLaptop.appendChild(document.createTextNode(laptop.title));
    elLaptops.appendChild(elLaptop);
}

//Changing data based on laptop menu change
const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    elPrice.innerText = selectedLaptop.price;
    elDescription.innerText = selectedLaptop.description;
    elTitle.innerText = selectedLaptop.title;
    elSpecs.innerText = selectedLaptop.specs;
    elImage.src = image + selectedLaptop.image;
}

//Buying a laptop - you can now get another loan.
const handleBuyLaptop = () => {
    if(balance >= elPrice.innerText) {
        balance -= elPrice.innerText;
        alert(`YOU ARE NOW THE NEW OWNER OF ${elTitle.innerText}`);
        elBalance.innerText = `Balance: ${balance} Kr.`;
        hasLoan = false;
    } else {
        alert(`You dont have enough money to buy ${elTitle.innerText}`);
    }


}

elLaptops.addEventListener("change", handleLaptopMenuChange);
btnLoan.addEventListener("click", handleBalance);
btnWork.addEventListener("click", handleWorkButton);
btnRepayLoan.addEventListener("click", handleRepayLoan);
btnBank.addEventListener("click", handleBankTransfer);
btnBuy.addEventListener("click", handleBuyLaptop);